#!/usr/bin/env ruby
require "bundler/setup"
require "yaml"

module Gitlab
  module Team
    class Member
      attr_accessor :details

      def initialize(file_location)
        @details = read_details_from_file(file_location)
      end

      def name
        details.fetch("name", "")
      end

      def projects
        details.fetch("projects", {})
      end

      def engineering?
        details.fetch("role", "dummy").scan(/(job-families\/engineering)/).any?
      end

      def engineering_manager?
        engineering? && details.fetch("role", "dummy").scan(/(\w* Engineering Manager|Engineering Manager, )/).any?
      end

      def backend_engineer?
        details.fetch("role", "dummy").scan(/(Backend Engineer)/).any?
      end

      def frontend_engineer?
        details.fetch("role", "dummy").scan(/(Frontend Engineer)/).any?
      end

      def senior_engineer?
        details.fetch("role", "dummy").scan(/(Senior) \w+ (Engineer)/).any?
      end

      def staff_engineer?
        details.fetch("role", "dummy").scan(/(Staff) \w* (Engineer)/).any?
      end

      def fellow_engineer?
        details.fetch("role", "dummy").scan(/(Fellow)/).any?
      end

      def intermediate_engineer?
        engineering? && !senior_engineer? && !staff_engineer? && !fellow_engineer? && !engineering_manager?
      end

      def intermediate_backend_engineer?
        backend_engineer? && intermediate_engineer?
      end

      def senior_backend_engineer?
        backend_engineer? && senior_engineer?
      end

      def trainee_maintainer?
        projects.select{|key, value| search(value, /trainee_maintainer/) }.any?
      end

      def maintainer?
        projects.select{|key, value| search(value, /^maintainer/) }.any?
      end

      private

      def read_details_from_file(file_location)
        @details ||= YAML.load(File.read(file_location))
      rescue
        puts "Unable to parse #{file_location}"
        return @details ||= {}
      end

      def search(value, regex)
        if value.is_a?(String)
          value.scan(regex).any?
        elsif value.is_a?(Array)
          value.grep(regex).any?
        else
          false
        end
      end
    end

    class Analyzer
      attr_accessor :members

      def initialize(directory)
        @members = []
        read_all_members_from_directory(directory)
      end

      def intermediates
        members.select{ |m| m.intermediate_engineer? }
      end

      def seniors
        members.select{ |m| m.senior_engineer? }
      end

      def maintainers
        members.select{ |m| m.maintainer? }
      end

      def trainee_maintainers
        members.select{ |m| m.trainee_maintainer? }
      end

      def intermediate_backend_engineers
        members.select{ |m| m.intermediate_backend_engineer? }
      end

      def intermediate_backend_maintainers
        members.select{ |m| m.maintainer? && m.intermediate_backend_engineer? }.flatten.uniq{ |i| i.details["gitlab"] }
      end

      def senior_maintainers
        members.select{ |m| m.maintainer? && m.senior_engineer? }.flatten.uniq{ |i| i.details["gitlab"] }
      end

      def engineering_managers
        members.select{ |m| m.engineering_manager? }
      end

      private

      def all_team_files(directory)
        Dir.glob(directory).reject! {|fn| File.directory?(fn) }
      end

      def read_all_members_from_directory(directory)
        all_team_files(directory).each do |file|
          puts "inspecting #{file}"
          members << Team::Member.new(file)
        end
      end
    end
  end
end
