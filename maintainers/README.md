# Maintainers

## Rationale

In discussions with @jagood about the number of project maintainers amongst the different seniority levels. The output here might be relevant to the ongoing discussion about [maintainer status as a requirement for senior+ engineering roles](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/106942).

## Usage

1. start a new IRB session `irb -r ./gitlab.rb`
2. start a new analysis with the entire team directory `analysis = Gitlab::Team::Analyzer.new("/Users/chase/work/www-gitlab-com/data/team_members/person/**/*")`
3. count the number of intermediate maintainers `analysis.intermediate_backend_maintainers.count`

