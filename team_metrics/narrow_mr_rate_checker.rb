require "bundler/setup"
require "httparty"
require "date"
require "csv"
require "tty-table"
require "tty-prompt"
require "rainbow/refinement"
require "perfect_toml"
using Rainbow

module Team
  team = PerfectTOML.load_file("team.toml", symbolize_names: true)
  MEMBERS = team[:members].freeze
end

module MRHarvester
  API_KEY = ENV.fetch("GITLAB_API_KEY")
  BASE_URL = "https://gitlab.com/api/v4/merge_requests"
  BASE_QUERY = {
    "author_username" => "",
    "created_after" => "", # 2020-01-01T00:00:00Z
    "scope" => "all",
    "state" => "merged"
    # "created_before" => "" #2020-02-01T00:00:00Z
  }
  BASE_HEADERS = {
    "PRIVATE-TOKEN" => API_KEY
  }

  # HEAD /merge_requests?query
  def harvest_meta(user, month = Date::MONTHNAMES[Date.today.month])
    # I suspect that we'll have to actually change the request type so that we can reject projects that are not within this CSV: https://gitlab.com/gitlab-data/analytics/blob/master/transform%2Fsnowflake-dbt%2Fdata%2Fprojects_part_of_product.csv
    # Also not sure that the combination of created_after and created_before is returning a list of MRs that's accurate for what we want. created_before: DateTime.parse(month).next_month.iso8601
    results = HTTParty.head(BASE_URL,
      query: BASE_QUERY.merge(author_username: user, created_after: DateTime.parse(month).iso8601),
      follow_redirects: true,
      maintain_method_across_redirects: true,
      headers: BASE_HEADERS)
    puts "#{user}: #{results["x-total"]}"
    results
  end

  # GET /merge_requests?query
  # if successful, returns an array of hashes
  def harvest_mrs(user, start_month = Date::MONTHNAMES[Date.today.month])
    Enumerator.new do |yielder|
      page = 1

      loop do
        start = DateTime.parse(start_month).iso8601
        results = HTTParty.get(BASE_URL,
          query: BASE_QUERY.merge(author_username: user, created_after: start, page: page, per_page: 100),
          follow_redirects: true,
          maintain_method_across_redirects: true,
          headers: BASE_HEADERS)

        if results.success? && !JSON.parse(results.body).empty?
          results.map { |item| yielder << item }
          page += 1
        else
          raise StopIteration
        end
      end
    end.lazy
  end
end

class NarrowMRRateChecker
  prepend Team
  prepend MRHarvester

  def initialize(month = Date::MONTHNAMES[Date.today.month])
    @month = month
    @project_ids = load_projects
    @merge_requests = [] # not great. what's returned by `load_merge_requests` is an array of hashes
    display_setup
    load_merge_requests
  end

  def calculate_rate
    get_total_mrs / (MEMBERS.length.to_f - 1) # remove kos due to the war in Ukraine
  end

  def calculate_limited_rate
    @merge_requests.size / (MEMBERS.length.to_f - 1) # remove kos due to the war in Ukraine
  end

  def load_merge_requests
    @merge_requests = get_limited_mrs.flatten
  end

  def print_limited_mrs
    temp_mr_ary = []
    headers = ["Title", "Created", "Merged", "Milestone", "Author"]
    @merge_requests.each { |mr| temp_mr_ary << [pretty_title(mr["title"]), pretty_date(mr["created_at"]), pretty_date(mr["merged_at"]), pretty_milestone(mr), mr["author"]["name"]] }
    table = TTY::Table.new(header: headers, rows: temp_mr_ary)
    renderer = TTY::Table::Renderer::ASCII.new(table)
    puts renderer.render
  end

  private

  def display_setup
    puts ">>> Investigating MRs from #{pretty_date(Date.parse(@month))} to #{pretty_date(next_month)}".yellow
  end

  def pretty_date(date)
    Date.parse(date).strftime("%Y-%m-%d")
  end

  def pretty_title(title)
    if title.size < 80
      return title
    end

    "#{title[0..76]}..."
  end

  def pretty_milestone(mr)
    mr.dig("milestone", "title") || "empty"
  end

  def next_month
    Date.parse(@month).next_month
  end

  def get_total_mrs(team: MEMBERS, month: @month)
    team.map { |member| harvest_meta(member, month)["x-total"].to_f }.reduce(0, :+)
  end

  def get_limited_mrs(team: MEMBERS, month: @month)
    team.map { |member| harvest_mrs(member, @month).to_a.select { |mr| @project_ids.include?(mr["project_id"].to_s) && (Date.parse(mr["merged_at"]) >= Date.parse(@month)) && (Date.parse(mr["merged_at"]) <= next_month) } }
  end

  # NOTE(csouthard): Consider caching this list into a local CSV or with something like pstore
  def load_projects
    response = HTTParty.get("https://gitlab.com/gitlab-data/analytics/-/raw/master/transform/snowflake-dbt/data/projects_part_of_product.csv")
    CSV.parse(response.body, headers: true)["project_id"]
  end
end

prompt = TTY::Prompt.new
month = prompt.ask("What month would you like to investigate?", default: Date::MONTHNAMES[Date.today.month]) do |q|
  q.required true
  q.modify :capitalize
  q.validate ->(input) { Date::MONTHNAMES.compact.include?(input.capitalize) }
end

utilization_merge_requests = NarrowMRRateChecker.new(month)
utilization_merge_requests.print_limited_mrs
puts "Average: #{utilization_merge_requests.calculate_limited_rate}"
