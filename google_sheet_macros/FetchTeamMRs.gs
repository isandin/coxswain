function updateMRSpreadsheetSubTeam() {
  function promptForAPIKey() {
    var ui = SpreadsheetApp.getUi();
    var response = ui.prompt('Let\'s get started.', 'Please supply your API key?', ui.ButtonSet.OK_CANCEL);
    
    if (response.getSelectedButton() == ui.Button.OK) {
      return response.getResponseText().trim();
    } else if (response.getSelectedButton() == ui.Button.CANCEL) {
      Logger.log('The user didn\'t want to provide an API key.');
      throw new Error("Cannot proceed without an API key!");
    } 
  }

 function formatDate(date) {
   if(date == null){
     return ""
   } else {
     var d = new Date(date),
         month = '' + (d.getMonth() + 1),
         day = '' + d.getDate(),
         year = d.getFullYear();

     if (month.length < 2) month = '0' + month;
     if (day.length < 2) day = '0' + day;

     return [year, month, day].join('-');
   }
 }

 function writeSpreadsheet(username, apiKey){
   var data = [];
   
   for (var page = 1; page < 10; page++) {
     var url = 'https://gitlab.com/api/v4/merge_requests?per_page=100&author_username=' + username + '&page=' + page + '&scope=all';
     var options = {
      "headers" : {
        "Authorization": "Bearer " + apiKey,
        "cache-control": "no-cache"
      }
     };
     var response = UrlFetchApp.fetch(url, options);
     // Parse the JSON reply
     var json = response.getContentText();
     data = data.concat(JSON.parse(json));
     if (json.length < 100 ) {
       break;
     }
   }

   var ss = SpreadsheetApp.getActiveSpreadsheet();
   var sheet = ss.getSheetByName(username)
   if (sheet == null) {
     sheet = ss.insertSheet(username);
   }

   var output = [];
   var columns = [
     "Title",
     "URL",
     "Created At",
     "Merged At",
     "State",
     "Created At Week",
     "Project ID"
   ]
   output.push(columns);

   data.forEach(function(elem,i) {
     output.push(
       [
         elem["title"],
         elem["web_url"],
         formatDate(elem["created_at"]),
         formatDate(elem["merged_at"]),
         elem["state"],
         '=C' + (i+2) + ' - WEEKDAY(C' + (i+2) + ',2) + 1',
         elem["project_id"],
       ]
     );
   });

   var len = output.length;

   // clear any previous content
   sheet.getRange(1,1,500,columns.length).clearContent();

   // paste in the values
   sheet.getRange(1,1,len,columns.length).setValues(output);
 }

 var engineers = [
   'csouthard',
   'jagood',
   'ebaque',
   'krasio',
   'vij'
 ]

 var apiKey = promptForAPIKey();
 
 engineers.forEach(function(engineer,i) {
   writeSpreadsheet(engineer, apiKey);
 })

}