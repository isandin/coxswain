# Google Sheet Macro

## Overview

[This macro](google_sheet_macros/FetchTeamMRs.gs) has been shared around for a while. It will query the GitLab Merge Reques API and create a new tab for each team member with the results. 

## Requirements

- Google Sheets access
- GitLab API key with sufficient privileges to read MRs including confidential MRs

## Usage

- customize this macro with a text editor with your [team member](google_sheet_macros/FetchTeamMRs.gs#L91) list
- create/open a Google Sheet document
- follow these instructions for [editing a macro](https://support.google.com/docs/answer/7665004?hl=en&co=GENIE.Platform%3DDesktop)
- paste in your customized version of this macro
- run the macro by selecting it from the Extensions > Macros menu
- enter your API key
- enjoy

## Resources

- [macro docs](https://developers.google.com/apps-script/guides/sheets/macros)
