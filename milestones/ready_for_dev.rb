require "httparty"

module IssueHarvester
  def get_issues_from_list(list_id)
    # NOTE(chaserx): there isn't a published API endpoint, but there's this that the UI pulls. Seems shortsighted.
    url = "https://gitlab.com/-/boards/1489558/lists/#{list_id}/issues?id=#{list_id}&label_name=cicd%3A%3Aactive%2Cgroup%3A%3Aprogressive%20delivery&page=1"
    HTTParty.get(url).tap do |response|
      raise ConnectionFailed if response.code.to_i != 200

      JSON.parse(response.body)
    end
  end

  def print_formatted_issues(issues)
    issues.each do |issue|
      url = "https://gitlab.com#{issue["real_path"]}"
      puts "- [ ] [#{issue["title"]}](#{url}) - (#{issue["labels"].map {|label| label["title"]} }) [#{issue["assignees"].map { |assingee| assingee["username"]}.join(", ")}]"
    end
  end
end

class ReadyForDev
  extend IssueHarvester

  def self.run!
    full_response = get_issues_from_list(4229491)
    issues = full_response.parsed_response['issues']
    size = full_response.parsed_response['size']
    weight = full_response.parsed_response['total_weight']
    puts "*Ready for Dev*"
    puts "_TOTAL ISSUES_: #{size}"
    puts "_TOTAL Weight_: #{weight}"
    print_formatted_issues(issues)
  end
end

ReadyForDev.run!
