# milestones

The following are a collection of scripts for details about issues with various `workflow::x` labels. 

#### Usage

These were moved recently. They need to be updated either have an internal Gemfile configuration block like that in `bin/capacity` or include a Gemfile in this directory. 

- [in_dev.rb](./in_dev.rb): attempts to collect all of the issues that are labeled with `workflow::in dev` from a list (issue board). The URLs need to be updated with the proper group labels you're interested in. 
- [ready_for_development.rb](./ready_for_development.rb): similar in functionality and required configuration as `in_dev.rb`, but targets the `workflow::ready_for_development` label. 
