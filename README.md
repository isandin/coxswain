```
cox·swain
/ˈkäks(ə)n/

noun
the steersman of a ship's boat, lifeboat, racing boat, or other boat.
```

#### Description

I was thinking about different kinds of teams, how they're organized, and the roles therein. In rowing/crew, there's a person at the back of the boat who coordinates the power and rhythm of the team. Here, I'm assembling some scripts to aid in my team coordination.

#### Usage

##### Requirements

* Ruby 2.5+ (ideally Ruby 3+)
* GitLab API key (personal access token) with rights to read issues and merge requests

##### Getting Started

* copy the .env.template to .env and replace the PRIVATE_TOKEN with your own GitLab API token. That token will need to have rights to issues and merge requests.
* run the getting_started script to install pre-commit hooks and run bundle command `./bin/get_started`

**Structure**

Each mini-tool is contained in it's own directory. Most of these are one-off tools and are put together quickly. As a result, there is some variability in stylistic conventions and dependency management where some projects may have a separate `Gemfile`.

#### Mini-tools

* [spillover](spillover/README.md): Getting issues that have spilled over for a given milestone
* [capacity](bin/capacity): get a list of issues, weights, and other milestone data
* [maintainers](maintainers/README.md): IRB session for analyzing team member files that roughly counts maintainers
* [log_parser](log_parser/README.nd): information processing JSON logs and conversion tools to CSV
* [milestones](milestones/README.md): various outdated scripts to format a list of issues by workflow label
* [markdown](markdown/README.md): various small markdown tools to avoid tedium
* [team metrics](team_metrics/README.md): unofficial MR rate inspection using the GitLab API

#### Style, Linting, and Formatting

In an effort to maintiain some consistency across these one-offs and bespoke mini-tools, the project relies on the [Standard](https://github.com/testdouble/standard) gem. It's part of the main [Gemfile](./Gemfile) and with the `get_started` script is it called from a pre-commit hook.
