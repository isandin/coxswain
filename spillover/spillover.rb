#!/usr/bin/env ruby
require "bundler/setup"
require 'dotenv/load'
require "dry/cli"
require "httparty"
require "highline"
require "erb"

module Spillover
  module CLI
    module Commands
      extend Dry::CLI::Registry

      class Version < Dry::CLI::Command
        desc "Print version"

        def call(*)
          puts "0.1"
        end
      end

      class Fetch < Dry::CLI::Command
        desc "Returns a formatted list of issues with just the Title, URL, labels, and assingees for use in reporting."

        def call(**)
          opts = get_options
          Issues::Client.new(opts[:milestone], opts[:stage_label], opts[:team_label], opts[:other_labels]).run!
        end

        def get_options
          ui = HighLine.new
          milestone = ui.ask("Which milestone?  ")
          stage_label = ui.ask("Which section label?  ") { |q| q.default = "devops::release" }
          team_label = ui.ask("Which team label?  ") { |q| q.default = "group::progressive delivery" }
          other_labels = ui.ask("Other labels?  ")
          Hash[milestone: milestone, stage_label: stage_label, team_label: team_label, other_labels: other_labels]
        end
      end

      register "version", Version, aliases: ["v", "-v", "--version"]
      register "fetch",   Fetch
    end
  end

  module Issues
    class Client
      include ERB::Util
      ConnectionFailed = Class.new(StandardError)

      Dotenv.require_keys("PRIVATE_TOKEN")

      BASE_URL = 'https://gitlab.com/api/v4'
      PRIVATE_TOKEN = ENV.fetch("PRIVATE_TOKEN")
      PER_PAGE = 100
      HEADERS = { 'Private-Token': PRIVATE_TOKEN }
      PATH = "#{BASE_URL}/issues?"

      Issue = Struct.new(:iid, :project_id, :title, :web_url, :labels, :assignees)

      attr_accessor :milestone, :stage, :team, :other, :data

      def initialize(milestone, stage, team, other="")
        @milestone = milestone
        @stage = stage
        @team = team
        @other = other
        @data = []
      end

      def print_formatted_issues
        issues = fetch_paginated_data.map { |response| Issue.new(response["iid"], response["project_id"], response["title"], response["web_url"], response["labels"], response["assignees"]) }

        issues.each do |issue|
          puts "- [ ] #{issue["title"]} - #{issue["web_url"]} (#{issue["labels"]}) [#{issue["assignees"].map { |assingee| assingee["username"]}.join(", ")}]"
        end
      end

      def format_assignees
      end

      def html_escape_options
      end

      def labels
        [stage,team,other].delete_if(&:empty?).join(",")
      end

      def additional_query_string_params_from_attrs
        "labels=#{html_escape(labels)}&milestone=#{milestone}"
      end

      def fetch_paginated_data
        Enumerator.new do |yielder|
          page = 1

          loop do
            results = fetch_data(page)

            if results.success? && !JSON.parse(results.body).empty?
              results.map { |item| yielder << item }
              page += 1
            else
              raise StopIteration
            end
          end
        end.lazy
      end

      def fetch_data(page)
        HTTParty.get("#{PATH}&per_page=#{PER_PAGE}&page=#{page}&state=opened&scope=all&#{additional_query_string_params_from_attrs}",
          follow_redirects: true,
          maintain_method_across_redirects: true,
          headers: HEADERS)
      end

      def run!
        print_formatted_issues
      end
    end
  end
end
