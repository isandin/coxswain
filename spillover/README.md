# Spillover

Spillover: `a backlog item that doesn't meet the Definition of Done (DoD)`

Get a formatted list of issues spilled over for a given milestone - unfinished work that may need to transition into the next milestone depending on priority.

Run: `./bin/spillover fetch`

See `/spillover/spillover.rb` for more.

![](../gifs/demo.gif)
