# log parsing

The following some tools to convert JSON logs into CSV for sharing with other team members who want to manipulate data in tabular form. 

## Log filtering with jq

The [jq](https://stedolan.github.io/jq/) tool is pretty powerful JSON parsing tool.

jq supports iso dates natively and you can select based on that value. The following is an example of filtering between two dates from a formatted log files.

```shell
cat ~/Downloads/downloaded-logs-20220825-190442.json | jq -c '[ .[] | select((.timestamp >= "2022-08-23T14:07:00Z") and (.timestamp <= "2022-08-23T15:40:00Z")) | select(.jsonPayload.amendment_options.amendable|contains("Gitlab Storage 10GB")) ]' | jq -c '[ .[] | { customerId: .jsonPayload.customer_id, subscriptionName: .jsonPayload.subscription_name, timestamp: .timestamp }]' > filtered_list.json
```

Piping the json data from one jq to another seemed to work fine. Below, is the invocation where the formatted output is the last item.

```shell
cat ~/Downloads/downloaded-logs-20220825-190442.json | jq -c '[ .[] | select((.timestamp >= "2022-08-23T14:07:00Z") and (.timestamp <= "2022-08-23T15:40:00Z")) | select(.jsonPayload.amendment_options.amendable|contains("Gitlab Storage 10GB")) ] | [ .[] | { customerId: .jsonPayload.customer_id, subscriptionName: .jsonPayload.subscription_name, timestamp: .timestamp }]' > filtered_list.json
```


## JSON to CSV Usage

Customize the code for filenames. This could be easily modified for ARGV or similar passing but this was a one off script. 

`ruby ./json_to_csv.rb`


