# Markdown tools

A blatant attempt to write code to avoid the tedium of it all.

- [csv_to_md.rb](./csv_to_md.rb): takes CSV path as argument; assumes `Title` and `URL` headers like you would get when exporting a list of Issues from GitLab. `ruby ./csv_to_md.rb /path/to/file/report.csv`
